import express, { Application } from 'express';
import Routes from './api/api'
export class App {

	app: Application

	constructor(private port?: number | string) {
		this.app = express();
		this.settings();
		this.middlewares();
		this.routes();
	}

	private settings() {
		this.app.set('port', this.port || process.env.PORT || 8080);
	}

	private middlewares() {
		this.app.use(express.json());
	}

	async listen(): Promise<void> {
		await this.app.listen(this.app.get('port'));
		console.log('Server running on port', this.app.get('port'));
	}

	private routes() {
		this.app.use('/api/v1', Routes);
	}

}