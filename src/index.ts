import { App } from './app';
import { initScheduler } from "./scheduler/cronVehicles"

async function main() {
	const app = new App(8080);
	await app.listen();
}

initScheduler();

main();