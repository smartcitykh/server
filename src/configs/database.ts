import { getConfig } from './config';
const mssql = require('mssql');
const config = getConfig('mssql_config_');

const poolConfig = {
	user: config.mssql.user,
	password: config.mssql.password,
	server: config.mssql.url,
	database: config.mssql.database
}

const pool = new mssql.ConnectionPool(poolConfig);
pool.connect();

export default pool;